import Vue from 'vue'

// import element-ui css before App and router to ensure the css priority
import ElementUI from 'element-ui'
import './style/element-variables.scss'
import './style/roboto-font/style.css' // TODO: move this to global css file
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

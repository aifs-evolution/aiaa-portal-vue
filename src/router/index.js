import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: '/explore/project'
  },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../components/login/Login.vue')
  },
  {
    path: '/monitor',
    name: 'monitor',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Monitor.vue')
  },
  {
    path: '/explore',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    children: [
      {
        path: 'project',
        name: 'explore-project',
        component: () => import(/* webpackChunkName: "frontStage-home" */'../components/explore/Explore.vue')
      }
    ],
    beforeEnter: beforeEnterManagementHandler,
    component: () => import(/* webpackChunkName: "about" */ '../views/Management.vue')
  },
  {
    path: '/:project',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    children: [
      {
        path: '',
        name: 'project',
        children: [
          {
            path: 'overview',
            name: 'project-overview',
            component: () => import(/* webpackChunkName: "frontStage-home" */'../components/project/Overview.vue')
          },
          {
            path: 'user',
            name: 'project-user',
            component: () => import(/* webpackChunkName: "frontStage-home" */'../components/project/User.vue')
          },
          {
            path: 'monitor',
            name: 'project-monitor',
            component: () => import(/* webpackChunkName: "frontStage-home" */'../components/project/Monitor.vue')
          },
          {
            path: 'monitor/:monitor',
            name: 'project-monitor-detail',
            component: () => import(/* webpackChunkName: "frontStage-home" */'../components/project/monitor/Detail.vue')
          },
          {
            path: 'system',
            name: 'project-system',
            component: () => import(/* webpackChunkName: "frontStage-home" */'../components/project/System.vue')
          },
          {
            path: 'iapp',
            name: 'project-iapp',
            component: () => import(/* webpackChunkName: "frontStage-home" */'../components/project/IAPP.vue')
          },
          {
            path: 'setting',
            name: 'project-setting',
            component: () => import(/* webpackChunkName: "frontStage-home" */'../components/project/Setting.vue')
          }
        ],
        component: () => import(/* webpackChunkName: "frontStage-home" */'../components/project/Project.vue')
      }
    ],
    beforeEnter: beforeEnterManagementHandler,
    component: () => import(/* webpackChunkName: "about" */ '../views/Management.vue')
  }
]

function beforeEnterManagementHandler (to, from, next) {
  if (localStorage.getItem('Authorized') && localStorage.getItem('Authorized') === 'OK') {
    next()
  } else {
    next({ name: 'login' })
  }
}

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
